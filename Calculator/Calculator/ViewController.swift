//
//  ViewController.swift
//  Calculator
//
//  Created by Bob on 2019/8/29.
//  Copyright © 2019 NTUTUTL. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var mLblNumber: UILabel!
    @IBOutlet weak var mLblOperation: UILabel!
    @IBOutlet weak var mBtnClear: DesignButton!
    
    var nowNumber: Double = 0
    var previousNumber: Double = 0
    var calculate = false
    
    var operation = Operation.none {
        willSet {
            switch newValue {
            case .addition:
                self.nowOperationTxt = "+"
                break
            case .subtraction:
                self.nowOperationTxt = "-"
                break
            case .multiplication:
                self.nowOperationTxt = "*"
                break
            case .division:
                self.nowOperationTxt = "/"
                break
            default :
                self.nowOperationTxt = " "
                break
            }
        }
    }
    
    var nowOperationTxt = String() {
        willSet {
            mLblOperation.text = newValue
        }
    }
    
    var answer = Double() {
        willSet {
            if round(newValue) == newValue {
                print(Int(newValue))
                mLblNumber.text = "\(Int(newValue))"
            }else {
                let value = "\(newValue)"
                let newVal = String(value.prefix(12))
                mLblNumber.text = newVal
            }
            nowNumber = newValue
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func numbersAction(_ sender: Any) {
        let s = sender as! UIButton
        let input = s.currentTitle
        
        
        if calculate || mLblNumber.text == "0" {
            mLblNumber.text = input!
            calculate = false
        }else {
            if let count = mLblNumber.text?.count, count < 12 {
                mLblNumber.text = mLblNumber.text! + input!
            }
        }
        
        nowNumber = Double(mLblNumber.text!)!
        mBtnClear.setTitle("C", for: .normal)
    }
    
    @IBAction func clearAction(_ sender: Any) {
        mLblNumber.text = "0"
        if operation != .none {
            mLblOperation.text = "CLEAR"
        }else {
            operation = .none
            mLblOperation.text = "All CLEAR"
        }
    }
    
    @IBAction func signAction(_ sender: Any) {
        if var lbl = mLblNumber.text, lbl.first == "-" {
            lbl.removeFirst()
            mLblNumber.text = lbl
        }else {
            mLblNumber.text = "-" + mLblNumber.text!
        }
        
        nowNumber = Double(mLblNumber.text!)!
    }
    
    @IBAction func percentAction(_ sender: Any) {
        if let newValue = Double(mLblNumber.text!) {
            mLblNumber.text = "\(newValue / 100)"
        }
        
        nowNumber = Double(mLblNumber.text!)!
    }
    
    @IBAction func dotAction(_ sender: Any) {
        if let lbl = mLblNumber.text, !lbl.contains(".") {
            mLblNumber.text = lbl + "."
        }
        
        nowNumber = Double(mLblNumber.text!)!
    }
    
    @IBAction func divisionAction(_ sender: Any) {
        assignment(action: Operation.division)
    }
    
    @IBAction func miltiplicationAction(_ sender: Any) {
        assignment(action: Operation.multiplication)
    }
    
    @IBAction func subtractionAction(_ sender: Any) {
        assignment(action: Operation.subtraction)
    }
    
    @IBAction func additionAction(_ sender: Any) {
        assignment(action: Operation.addition)
    }
    
    @IBAction func answerAction(_ sender: Any) {
        calculator()
        operation = .none
        mBtnClear.setTitle("AC", for: .normal)
        print("previous \(previousNumber), now \(nowNumber)")
    }
    
    func assignment(action: Operation) {
        if !calculate {
            calculator()
            operation = action
        }
        previousNumber = nowNumber
        calculate = true
    }
    
    func calculator() {
        switch self.operation {
        case .addition:
            answer = previousNumber + nowNumber
            break
        case .subtraction:
            answer = previousNumber - nowNumber
            break
        case .multiplication:
            answer = previousNumber * nowNumber
            break
        case .division:
            answer = previousNumber / nowNumber
            break
        default:
            break
        }
        
        
    }
}
