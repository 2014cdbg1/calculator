//
//  Opertion.swift
//  Calculator
//
//  Created by Bob on 2019/8/29.
//  Copyright © 2019 NTUTUTL. All rights reserved.
//

import Foundation

enum Operation {
    case addition
    case subtraction
    case multiplication
    case division
    case none
}
