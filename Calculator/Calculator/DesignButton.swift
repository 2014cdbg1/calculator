//
//  DesignButton.swift
//  Calculator
//
//  Created by Bob on 2019/8/29.
//  Copyright © 2019 NTUTUTL. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable

class DesignButton: UIButton {
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
}
